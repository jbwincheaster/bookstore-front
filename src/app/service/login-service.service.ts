import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppConstants} from '../app-constants';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginServiceService {

  constructor(private http: HttpClient, private router: Router) {
  }

  // tslint:disable-next-line:typedef
  login(usuario) {
    return this.http.post(AppConstants.baseLogin, JSON.stringify(usuario)).subscribe(data => {
        const token = JSON.parse(JSON.stringify(data)).Authorization.split(' ')[1];
        localStorage.setItem('token', token);
        this.router.navigate(['home']);
      },
      error => {
      alert('Login ou senha incorreto');
      }
    );
  }
}
